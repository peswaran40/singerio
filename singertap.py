import requests
import singer

LOGGER = singer.get_logger()

def fetch_reviews(url, token):
    headers = {
        'Authorization': f'Token 00832c3ccbe9758488816d853e5f749d28a59ed7'
    }

    response = requests.get(url, headers=headers)
    response.raise_for_status()

    return response.json()

def sync_reviews(config, state, catalog):
    token = config['token']
    page = 1

    while True:
        url = f"https://tool.nextgrowthlabs.com/aso-tool/api/v1/reviews/?appId=com.whatsapp&page={page}"
        LOGGER.info(f"Fetching reviews from URL: {url}")
        reviews = fetch_reviews(url, token)

        if not reviews:
            LOGGER.info("No more reviews found. Stopping sync.")
            break

        for review in reviews:
            singer.write_record('reviews', review)

        page += 1

    singer.write_state(state)

def main():
    config = singer.get_config()
    state = singer.State()

    catalog = singer.Catalog.load(config['catalog'])
    LOGGER.info(f"Selected streams: {catalog.get_selected_streams()}")

    sync_reviews(config, state, catalog)

if __name__ == '__main__':
    main()
